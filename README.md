# Node.js & Passport Login

This is a user login and registration app using Node.js, Express, Passport, Mongoose, EJS and some other packages.

### Version: 2.0.0

### Usage

```sh
$ npm install
```

```sh
$ npm start
# Or run with Nodemon
$ npm run dev

# Visit http://localhost:3000
```

### MongoDB

```sh
Open mongodb atlas and login or signup.
Create a Organization => Give Organization Name and Add Memeber if you want. => Next.
Create a New Project => Give Project Name and Add Member if you want. => Next.
Create a Cluster => Build Cluster.
Choose a Path => Shared Cluster.
Choose Cloud Provide => AWS.
Select Region.
Create Cluster.

Click connect.
Add IP Address.
Create UserName and Password.
Add your connection string into your application code.
Replace <password> with the Password for the same for <user> insteadOf User, and ensure all special characters are URL encoded.

Open "config/keys.js" and add your MongoDB URI, local or Atlas
```